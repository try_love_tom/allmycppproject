//
//  main.c
//  NaiveGaussElimination
//
//  Created by ChangTom on 2015/11/2.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <math.h>

int main( int argc, const char * argv[] )
{
    double matrixA[ 3 ][ 3 ] =
    {
        { 1, 2, 3 },
        { 2, 3, 4 },
        { 3, 4, 5 }
    };
    double matrixB[ 3 ] =
    {
        1,
        2,
        3
    };
    int col = sizeof( matrixA[ 0 ] ) / sizeof( double );
    int row = sizeof( matrixB ) / sizeof( double ) / col;
    
    if ( col != row)
    
    return 0;
}
