//
//  main.c
//  ParabolicInterpolation
//
//  Created by ChangTom on 2015/11/10.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <math.h>

double f( double x )
{
    return 2 * x + 3 / x;
}

double ParabolicInterpolation( double x1, double x2, double x3, double (*f)( double ) )
{
    double part1 = ( x2 - x1 ) * ( x2 - x1 ) * ( f( x2 ) - f( x3 ) ) - ( x2 - x3 ) * ( x2 - x3 ) * ( f( x2 ) - f( x1 ) );
    double part2 = ( x2 - x1 ) * ( f( x2 ) - f( x3 ) ) - ( x2 - x3 ) * ( f( x2 ) - f( x1 ) );
    double x4 = x2 - 0.5 *  part1 / part2;
    
    return x4;
}

int main( int argc, const char * argv[] )
{
    double x;
    double x1 = 0.1;
    double x2 = 0.5;
    double x3 = 5;
    
    for ( int i = 0; i < 10; ++i )
    {
        x = ParabolicInterpolation( x1, x2, x3, &f );
        printf( "f( %lf ) = %lf\n", x, f( x ) );
        
        if ( x > x2 )
        {
            x1 = x2;
            x2 = x;
        }
        else
        {
            x3 = x2;
            x2 = x;
        }
    }
    return 0;
}
