//
//  main.c
//  Bisection
//
//  Created by ChangTom on 2015/10/12.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <math.h>

double f( double x )
{
    return sin( x + cos( x ) ) / ( 1 + x * x );
}

double bisection( double a, double b, double (*f)( double ), int times )
{
    double fb = f( b ), fa = f( a ), c = 0.0, fc;
    
    if ( fa == 0 )
    {
        return a;  // a 就是根
    }
    
    if ( fb == 0 )
    {
        return b;  // b 就是根
    }
    
    while ( times-- )
    {
        c = ( a + b ) / 2;
        fc = f( c );
        if ( fc * fb > 0 ) // c 和 b 同邊
        {
            b = c;
            fb = fc;
        }
        else if ( fc * fa > 0 ) // c 和 a 同邊
        {
            a = c;
            fa = fc;
        }
        else
        {
            return c; // c 就是根
        }
    }
    
    return c;
}

int main( int argc, const char * argv[] )
{
    printf( "%f\n", bisection( 4, 6, &f, 100 ) );
    
    return 0;
}
