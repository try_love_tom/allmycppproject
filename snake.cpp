#include <iostream>
#include <ctime>
#define MAXPIX 64
#define MAXLEN 8
using namespace std;

struct Node
{
    int x, y;
};
void printMap( Node snake[ MAXPIX ], int snakeLen )
{
    int i, j, k;
    
    for ( i = 0; i < MAXLEN; i++ )
    {
        for ( j = 0; j < MAXLEN; j++ )
        {
            int r = 1;
            
            for ( k = 0; k <= snakeLen; k++ )
            {
                if ( ( snake[ k ].x == j ) && ( snake[ k ].y == i ) )
                {
                    r = 0;
                    if ( !k )
                        cout << 'f';
                    else if ( k == 1 )
                        cout << '@';
                    else
                        cout << '+';
                }
            }
            if ( r )
                cout << '_';
        }
        cout << endl;
    }
}
int go( Node snake[ MAXPIX ], int direction, int *snakeLen, int *point ) // return 0 == die;
{
    int tempX = snake[ *snakeLen ].x, tempY = snake[ *snakeLen ].y;
    int i, j;
    // shift snake
    for ( i = *snakeLen; i > 1; i-- )
    {
        snake[ i ].x = snake[ i - 1 ].x;
        snake[ i ].y = snake[ i - 1 ].y;
    }
    // move four direction
    switch ( direction )
    {
        case 0:
            if ( snake[ 1 ].y + 1 == MAXLEN )
                snake[ 1 ].y = 0;
            else
                snake[ 1 ].y += 1;
            break;
        case 1:
            if ( snake[ 1 ].y - 1 < 0 )
                snake[ 1 ].y = MAXLEN - 1;
            else
                snake[ 1 ].y -= 1;
            break;
        case 2:
            if ( snake[ 1 ].x - 1 < 0 )
                snake[ 1 ].x = MAXLEN - 1;
            else
                snake[ 1 ].x -= 1;
            break;
        case 3:
            if ( snake[ 1 ].x + 1 == MAXLEN )
                snake[ 1 ].x = 0;
            else
                snake[ 1 ].x += 1;
            break;
    }
    // die?
    for ( i = 2; i <= *snakeLen; i++ )
    {
        if ( ( snake[ 1 ].x == snake[ i ].x ) && ( snake[ 1 ].y == snake[ i ].y ) )
            return 0;
    }
    // eat food?
    if ( ( snake[ 1 ].x == snake[ 0 ].x ) && ( snake[ 1 ].y == snake[ 0 ].y ) )
    {
        *snakeLen += 1;
        *point += 100;
        int r = 0;
        
        do
        {
            r = 0;
            int x = rand() % MAXLEN;
            int y = rand() % MAXLEN;
            
            for ( j = 1; j <= *snakeLen; j++ )
            {
                if ( ( snake[ j ].x == x ) && ( snake[ j ].y == y ) )
                    r = 1;
            }
            if ( !r )
            {
                snake[ 0 ].y = y;
                snake[ 0 ].x = x;
            }
        } while ( r );
        snake[ *snakeLen ].x = tempX;
        snake[ *snakeLen ].y = tempY;
    }
    return 1;
}
int main()
{
    int direction = 0; // 0 down, 1 up, 2 left, 3 right
    int tempDirection = direction;
    int snakeLen = 4;
    Node snake[ MAXPIX ];
    int point = 0;
    
    // initial snake
    snake[ 1 ].x = 0;
    snake[ 1 ].y = 3;
    
    snake[ 2 ].x = 0;
    snake[ 2 ].y = 2;
    
    snake[ 3 ].x = 0;
    snake[ 3 ].y = 1;
    
    snake[ 4 ].x = 0;
    snake[ 4 ].y = 0;
    
    // initial food
    snake[ 0 ].x = rand() % MAXLEN;
    snake[ 0 ].y = rand() % MAXLEN;
    
    // game start
    do
    {
        cout << point << endl;
        printMap( snake, snakeLen );
        cin >> direction;
        if ( ( direction < 0 ) || ( direction > 3 ) )
            direction = tempDirection;
        if ( ( direction == 1 ) && ( tempDirection == 0 ) )
            direction = tempDirection;
        if ( ( direction == 0 ) && ( tempDirection == 1 ) )
            direction = tempDirection;
        if ( ( direction == 2 ) && ( tempDirection == 3 ) )
            direction = tempDirection;
        if ( ( direction == 3 ) && ( tempDirection == 2 ) )
            direction = tempDirection;
        tempDirection = direction;
    }
    while ( go( snake, direction, &snakeLen, &point ) );
    cout << point << endl;
    cout << "die" << endl;
    return 0;
}