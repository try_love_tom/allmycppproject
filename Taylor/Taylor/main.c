#include <stdio.h>
#include <math.h>

double myExp( double, double );
double mySin( double, double );
double myCos( double, double );
double myTan( double, double );
double findMySinError( double, int );

int main()
{
    double x, j;
    
    scanf( "%lf%lf", &x, &j );
    //printf( "exp( %lf ) = %lf\nmyExp( %lf ) = %lf\n", x, exp( x ), x, myExp( x, j ) );
    printf( "sin( %lf ) = %lf\nmySin( %lf ) = %lf\n", x, sin( x ), x, mySin( x, j ) );
    //printf( "cos( %lf ) = %lf\nmyCos( %lf ) = %lf\n", x, cos( x ), x, myCos( x, j ) );
    printf( "Error: %f\n", findMySinError( x, j ) );
    
    return 0;
}

// e ^ x 誤差小於 10 ^ j
double myExp( double x, double j )
{
    double factorial = 1;
    double counter = 2;
    double powerOfX = x;
    double target = pow( 10, j );
    double tempLast = 0;
    double temp = 1;
    int n = 0;
    
    if ( target >= 1 )
    {
        do
        {
            n++;
            tempLast = temp;
            temp += powerOfX / factorial;
            powerOfX *= x;
            factorial *= counter++;
        } while ( n <= j );
    }
    else
    {
        do
        {
            n++;
            tempLast = temp;
            temp += powerOfX / factorial;
            powerOfX *= x;
            factorial *= counter++;
        } while ( !( fabs( tempLast - temp ) < target ) );
        printf( "\nn = %d\n", n - 1 );
    }
    
    return tempLast;
}

double mySin( double x, double j )
{
    double factorial = 1;
    double counter = 1;
    double powerOfX = x;
    double target = pow( 10, j );
    double tempLast = 0;
    double temp = 0;
    
    int n = 0;
    
    if ( target >= 1 )
    {
        do
        {
            n++;
            temp += powerOfX / factorial;
            powerOfX = -powerOfX * x * x;
            factorial *= counter + 1;
            factorial *= counter + 2;
            counter += 2;
            tempLast = temp;
        } while ( n <= j );
    }
    else
    {
        do
        {
            n++;
            temp += powerOfX / factorial;
            powerOfX = -powerOfX * x * x;
            factorial *= counter + 1;
            factorial *= counter + 2;
            counter += 2;
            tempLast = temp;
        } while ( !( fabs( tempLast - temp ) < target ) );
        printf( "\nn = %d\n", n - 1 );
    }
    
    return tempLast;
}

double myCos( double x, double j )
{
    double factorial = 2;
    double counter = 2;
    double powerOfX = -x * x;
    double target = pow( 10, j );
    double tempLast = 0;
    double temp = 1;
    
    int n = 0;
    
    if ( j >= 1 )
    {
        do
        {
            n++;
            tempLast = temp;
            temp += powerOfX / factorial;
            powerOfX = -powerOfX * x * x;
            factorial *= counter + 1;
            factorial *= counter + 2;
            counter += 2;
        } while ( n <= j );
    }
    else
    {
        do
        {
            n++;
            tempLast = temp;
            temp += powerOfX / factorial;
            powerOfX = -powerOfX * x * x;
            factorial *= counter + 1;
            factorial *= counter + 2;
            counter += 2;
        } while ( !( fabs( tempLast - temp ) < target ) );
        printf( "\nn = %d\n", n - 1 );
    }
    
    return tempLast;
}

double findMySinError( double x, int n )
{
    return fabs( mySin( x, n ) - sin( x ) );
}
