四電子三甲 B10202012 張子晏

心得：
（1.618 ^ n）成長率，真的讓人難以忍受呀，
我做到59就需要一個多小時了。


Code:

//
//  main.c
//  FibonacciNumber
//
//  Created by ChangTom on 2015/10/7.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#define MAPPING

#ifdef MAPPING
#define N 10000
double map[ N ] = { 0 };
#endif

double fibonacci( int n )
{
    if ( n <= 0 )
    {
        return 0;
    }
    if ( n == 1 )
    {
        return 1;
    }
    if ( n == 2 )
    {
        return 1;
    }
#ifdef MAPPING
    if ( map[ n ] != 0 )
    {
        return map[ n ];
    }
    return map[ n ] = fibonacci( n - 1 ) + fibonacci( n - 2 );
#else
    return fibonacci( n - 1 ) + fibonacci( n - 2 );
#endif
}

int main( int argc, const char * argv[] )
{
    int counter = 0;
    double lastTime = 0;
    
    while ( 1 )
    {
        FILE *fp;
        clock_t t;
        double temp;
        double currentTime;
        
        fp = fopen( "log", "a") ;     /* open file pointer */
        
        counter++;
        t = clock();
        temp = fibonacci( counter );
        t = clock() - t;
        
        currentTime = ( (double)t ) / CLOCKS_PER_SEC;
        
        fprintf( fp, "Please type input size( n ): %d\n", counter );
        fprintf( fp, "Fibonacci( %d ) = %.0f\n", counter, temp );
        fprintf( fp, "Elapsed time: %lfsec\n", currentTime );
        fprintf( fp, "CurrentTime / LastTime = %f\n", currentTime / lastTime );
        
        printf( "Please type input size( n ): %d\n", counter );
        printf( "Fibonacci( %d ) = %.0f\n", counter, temp );
        printf( "Elapsed time: %lfsec\n", currentTime );
        printf( "CurrentTime / LastTime = %f\n", currentTime / lastTime );
        
        lastTime = currentTime;
        
        fclose( fp );
    }
    /*
     for ( int i = 1; i < N; ++i )
     {
     double temp0 = fibonacci( i );
     double temp1 = fibonacci( i + 1 );
     printf( "N = %d: %.10f\n", i, temp1 / temp0 - temp0 / temp1 );
     }
     */
    return 0;
}

Output:

Please type input size( n ): 1
Fibonacci( 1 ) = 1
Elapsed time: 0.000001sec
CurrentTime / LastTime = inf
Please type input size( n ): 2
Fibonacci( 2 ) = 1
Elapsed time: 0.000001sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 3
Fibonacci( 3 ) = 2
Elapsed time: 0.000001sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 4
Fibonacci( 4 ) = 3
Elapsed time: 0.000002sec
CurrentTime / LastTime = 2.000000
Please type input size( n ): 5
Fibonacci( 5 ) = 5
Elapsed time: 0.000001sec
CurrentTime / LastTime = 0.500000
Please type input size( n ): 6
Fibonacci( 6 ) = 8
Elapsed time: 0.000002sec
CurrentTime / LastTime = 2.000000
Please type input size( n ): 7
Fibonacci( 7 ) = 13
Elapsed time: 0.000001sec
CurrentTime / LastTime = 0.500000
Please type input size( n ): 8
Fibonacci( 8 ) = 21
Elapsed time: 0.000001sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 9
Fibonacci( 9 ) = 34
Elapsed time: 0.000001sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 10
Fibonacci( 10 ) = 55
Elapsed time: 0.000002sec
CurrentTime / LastTime = 2.000000
Please type input size( n ): 11
Fibonacci( 11 ) = 89
Elapsed time: 0.000002sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 12
Fibonacci( 12 ) = 144
Elapsed time: 0.000002sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 13
Fibonacci( 13 ) = 233
Elapsed time: 0.000002sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 14
Fibonacci( 14 ) = 377
Elapsed time: 0.000002sec
CurrentTime / LastTime = 1.000000
Please type input size( n ): 15
Fibonacci( 15 ) = 610
Elapsed time: 0.000005sec
CurrentTime / LastTime = 2.500000
Please type input size( n ): 16
Fibonacci( 16 ) = 987
Elapsed time: 0.000006sec
CurrentTime / LastTime = 1.200000
Please type input size( n ): 17
Fibonacci( 17 ) = 1597
Elapsed time: 0.000009sec
CurrentTime / LastTime = 1.500000
Please type input size( n ): 18
Fibonacci( 18 ) = 2584
Elapsed time: 0.000021sec
CurrentTime / LastTime = 2.333333
Please type input size( n ): 19
Fibonacci( 19 ) = 4181
Elapsed time: 0.000020sec
CurrentTime / LastTime = 0.952381
Please type input size( n ): 20
Fibonacci( 20 ) = 6765
Elapsed time: 0.000037sec
CurrentTime / LastTime = 1.850000
Please type input size( n ): 21
Fibonacci( 21 ) = 10946
Elapsed time: 0.000056sec
CurrentTime / LastTime = 1.513514
Please type input size( n ): 22
Fibonacci( 22 ) = 17711
Elapsed time: 0.000082sec
CurrentTime / LastTime = 1.464286
Please type input size( n ): 23
Fibonacci( 23 ) = 28657
Elapsed time: 0.000147sec
CurrentTime / LastTime = 1.792683
Please type input size( n ): 24
Fibonacci( 24 ) = 46368
Elapsed time: 0.000210sec
CurrentTime / LastTime = 1.428571
Please type input size( n ): 25
Fibonacci( 25 ) = 75025
Elapsed time: 0.000329sec
CurrentTime / LastTime = 1.566667
Please type input size( n ): 26
Fibonacci( 26 ) = 121393
Elapsed time: 0.000523sec
CurrentTime / LastTime = 1.589666
Please type input size( n ): 27
Fibonacci( 27 ) = 196418
Elapsed time: 0.000832sec
CurrentTime / LastTime = 1.590822
Please type input size( n ): 28
Fibonacci( 28 ) = 317811
Elapsed time: 0.001334sec
CurrentTime / LastTime = 1.603365
Please type input size( n ): 29
Fibonacci( 29 ) = 514229
Elapsed time: 0.002090sec
CurrentTime / LastTime = 1.566717
Please type input size( n ): 30
Fibonacci( 30 ) = 832040
Elapsed time: 0.003673sec
CurrentTime / LastTime = 1.757416
Please type input size( n ): 31
Fibonacci( 31 ) = 1346269
Elapsed time: 0.005952sec
CurrentTime / LastTime = 1.620474
Please type input size( n ): 32
Fibonacci( 32 ) = 2178309
Elapsed time: 0.008508sec
CurrentTime / LastTime = 1.429435
Please type input size( n ): 33
Fibonacci( 33 ) = 3524578
Elapsed time: 0.014844sec
CurrentTime / LastTime = 1.744711
Please type input size( n ): 34
Fibonacci( 34 ) = 5702887
Elapsed time: 0.023970sec
CurrentTime / LastTime = 1.614794
Please type input size( n ): 35
Fibonacci( 35 ) = 9227465
Elapsed time: 0.036546sec
CurrentTime / LastTime = 1.524656
Please type input size( n ): 36
Fibonacci( 36 ) = 14930352
Elapsed time: 0.057002sec
CurrentTime / LastTime = 1.559733
Please type input size( n ): 37
Fibonacci( 37 ) = 24157817
Elapsed time: 0.095941sec
CurrentTime / LastTime = 1.683116
Please type input size( n ): 38
Fibonacci( 38 ) = 39088169
Elapsed time: 0.148058sec
CurrentTime / LastTime = 1.543219
Please type input size( n ): 39
Fibonacci( 39 ) = 63245986
Elapsed time: 0.234555sec
CurrentTime / LastTime = 1.584210
Please type input size( n ): 40
Fibonacci( 40 ) = 102334155
Elapsed time: 0.373737sec
CurrentTime / LastTime = 1.593387
Please type input size( n ): 41
Fibonacci( 41 ) = 165580141
Elapsed time: 0.621235sec
CurrentTime / LastTime = 1.662225
Please type input size( n ): 42
Fibonacci( 42 ) = 267914296
Elapsed time: 1.045154sec
CurrentTime / LastTime = 1.682381
Please type input size( n ): 43
Fibonacci( 43 ) = 433494437
Elapsed time: 1.692698sec
CurrentTime / LastTime = 1.619568
Please type input size( n ): 44
Fibonacci( 44 ) = 701408733
Elapsed time: 2.683345sec
CurrentTime / LastTime = 1.585247
Please type input size( n ): 45
Fibonacci( 45 ) = 1134903170
Elapsed time: 4.386791sec
CurrentTime / LastTime = 1.634822
Please type input size( n ): 46
Fibonacci( 46 ) = 1836311903
Elapsed time: 7.011477sec
CurrentTime / LastTime = 1.598316
Please type input size( n ): 47
Fibonacci( 47 ) = 2971215073
Elapsed time: 11.446649sec
CurrentTime / LastTime = 1.632559
Please type input size( n ): 48
Fibonacci( 48 ) = 4807526976
Elapsed time: 18.101659sec
CurrentTime / LastTime = 1.581394
Please type input size( n ): 49
Fibonacci( 49 ) = 7778742049
Elapsed time: 30.313447sec
CurrentTime / LastTime = 1.674623
Please type input size( n ): 50
Fibonacci( 50 ) = 12586269025
Elapsed time: 48.569365sec
CurrentTime / LastTime = 1.602238
Please type input size( n ): 51
Fibonacci( 51 ) = 20365011074
Elapsed time: 79.743447sec
CurrentTime / LastTime = 1.641847
Please type input size( n ): 52
Fibonacci( 52 ) = 32951280099
Elapsed time: 123.534806sec
CurrentTime / LastTime = 1.549153
Please type input size( n ): 53
Fibonacci( 53 ) = 53316291173
Elapsed time: 209.431838sec
CurrentTime / LastTime = 1.695327
Please type input size( n ): 54
Fibonacci( 54 ) = 86267571272
Elapsed time: 347.185731sec
CurrentTime / LastTime = 1.657750
Please type input size( n ): 55
Fibonacci( 55 ) = 139583862445
Elapsed time: 528.523511sec
CurrentTime / LastTime = 1.522308
Please type input size( n ): 56
Fibonacci( 56 ) = 225851433717
Elapsed time: 826.658027sec
CurrentTime / LastTime = 1.564089
Please type input size( n ): 57
Fibonacci( 57 ) = 365435296162
Elapsed time: 1338.877465sec
CurrentTime / LastTime = 1.619627
Please type input size( n ): 58
Fibonacci( 58 ) = 591286729879
Elapsed time: 2285.534395sec
CurrentTime / LastTime = 1.707053
Please type input size( n ): 59
Fibonacci( 59 ) = 956722026041
Elapsed time: 3718.432224sec
CurrentTime / LastTime = 1.626942
