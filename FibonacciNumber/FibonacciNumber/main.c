//
//  main.c
//  FibonacciNumber
//
//  Created by ChangTom on 2015/10/7.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#define MAPPING

#ifdef MAPPING
#define N 10000
double map[ N ] = { 0 };
#endif

double fibonacci( int n )
{
    if ( n <= 0 )
    {
        return 0;
    }
    if ( n == 1 )
    {
        return 1;
    }
    if ( n == 2 )
    {
        return 1;
    }
#ifdef MAPPING
    if ( map[ n ] != 0 )
    {
        return map[ n ];
    }
    return map[ n ] = fibonacci( n - 1 ) + fibonacci( n - 2 );
#else
    return fibonacci( n - 1 ) + fibonacci( n - 2 );
#endif
}

int main( int argc, const char * argv[] )
{
    int counter = 0;
    double lastTime = 0;
    
    while ( 1 )
    {
        FILE *fp;
        clock_t t;
        double temp;
        double currentTime;
        
        fp = fopen( "log", "a") ;     /* open file pointer */
        
        counter++;
        t = clock();
        temp = fibonacci( counter );
        t = clock() - t;
        
        currentTime = ( (double)t ) / CLOCKS_PER_SEC;
        
        fprintf( fp, "Please type input size( n ): %d\n", counter );
        fprintf( fp, "Fibonacci( %d ) = %.0f\n", counter, temp );
        fprintf( fp, "Elapsed time: %lfsec\n", currentTime );
        fprintf( fp, "CurrentTime / LastTime = %f\n", currentTime / lastTime );
        
        printf( "Please type input size( n ): %d\n", counter );
        printf( "Fibonacci( %d ) = %.0f\n", counter, temp );
        printf( "Elapsed time: %lfsec\n", currentTime );
        printf( "CurrentTime / LastTime = %f\n", currentTime / lastTime );
        
        lastTime = currentTime;
        
        fclose( fp );
    }
    /*
     for ( int i = 1; i < N; ++i )
     {
     double temp0 = fibonacci( i );
     double temp1 = fibonacci( i + 1 );
     printf( "N = %d: %.10f\n", i, temp1 / temp0 - temp0 / temp1 );
     }
     */
    return 0;
}
