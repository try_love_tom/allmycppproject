//
//  main.cpp
//  LinearIndependence
//
//  Created by 張子晏 on 2015/6/27.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#include <iostream>
#include <vector>
#include <iomanip>

bool IsLI( std::vector< double > Ma[] );

int main( int argc, const char * argv[] )
{
    // insert code here...
    std::cout << "Hello, World!\n";
    
    std::vector< double > a;
    std::vector< double > b;
    std::vector< double > c;
    
    a.push_back( 1 );
    a.push_back( 0 );
    a.push_back( -2 );
    
    b.push_back( 2 );
    b.push_back( 1 );
    b.push_back( 0 );
    
    c.push_back( 3 );
    c.push_back( 2 );
    c.push_back( 0 );
    
    std::vector< double > Ma[ 3 ] = { a, b, c };
    
    std::cout << IsLI( Ma );
    
    return 0;
}

bool IsLI( std::vector< double > Ma[] )
{
    double x = 0, det = 1;
    
    for ( unsigned i = 0 ; i < Ma[ 0 ].size() - 1; i++ )
    {
        for ( unsigned j = i ; j < Ma[ 0 ].size() - 1; j++ )
        {
            if ( Ma[ i ][ i ] == 0 && Ma[ i + 1 ][ i ] != 0 )
            {
                std::vector< double > temp = Ma[ i + 1 ];
                Ma[ i + 1 ] = Ma[ i ];
                Ma[ i ] = temp;
            }
            x = ( Ma[ j + 1 ][ i ] / Ma[ i ][ i ] ) * -1;
            for ( unsigned k = 0; k < Ma[ 0 ].size(); k++ )
            {
                Ma[ j + 1 ][ k ] += Ma[ i ][ k ] * x;
            }
        }
    }
    
    
    for ( unsigned i = 0 ; i < Ma[ 0 ].size(); i++ )
    {
        for ( unsigned j = 0 ; j < Ma[ 0 ].size(); j++ )
        {
            std::cout << std::setw( 15 ) << Ma[ i ][ j ];
        }
        std::cout << std::endl;
    }
    
    
    for ( unsigned i = 0; i < Ma[ 0 ].size(); i++)
    {
        det = det * Ma[ i ][ i ];
    }
    if ( det == 0 )
    {
        return false;
    }
    return true;
}
