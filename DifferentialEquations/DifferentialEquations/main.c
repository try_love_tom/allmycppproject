//
//  main.c
//  DifferentialEquations
//
//  Created by ChangTom on 2015/10/19.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <math.h>

#define LAMBDA 0.5f

double f( double x )
{
    // return -2 * pow( x, -6 ) + 1.5 * pow( x, 4 ) + 10 * x + 2;
    return x * x * x + x - 1;
}

double df( double (*f)( double ) , double x, double precision )
{
    double result = 0.0f;
    double delta = pow( 10, -10 );
    double temp = 0.0f;
    
    do
    {
        temp = result;
        result = ( f( x + delta ) - f( x ) ) / ( ( x + delta ) - x );
        delta /= 2;
    } while ( fabs( temp - result ) >= precision );
    
    return result;
}

double root( double (*f)( double ) , double x, double precision )
{
    double x1 = 0.0f;
    
    while ( fabs( x - x1 ) >= precision )
    {
        x1 = x;
        x = x - f( x ) / df( f, x, precision );
        printf( "%lf\n", x );
    }
    
    return x;
}

int main( int argc, const char * argv[] )
{
    printf( "ans: %lf\n", root( &f, 1.0f, pow( 10, -3 ) ) );
    return 0;
}
