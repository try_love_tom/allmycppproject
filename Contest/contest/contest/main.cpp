#include <iostream>
#include <vector>
#include <math.h>
#define NEW2D( H, W, TYPE )(TYPE**)new2d( H, W, sizeof( TYPE ) )
using namespace std;

void* new2d( int h, int w, int size )
{
    void **p;
    
    p = (void**)new char[ h * sizeof( void* ) + h * w * size ];
    for ( int i = 0; i < h ; ++i )
    {
        p[ i ] = ( (char*)( p + h ) ) + i * w * size;
    }
    return p;
}

int main()
{
    double p;
    string input;
    vector<char> bdd;
    vector<char> stack;
    int size = 0;
    char last = '\0';
    int tableIndex = 0;
    bool **table;
    
    //cin >> p;
    cin >> input;
    for ( int i = 0; i < input.length(); ++i )
    {
        if ( input[ i ] > size )
        {
            size = input[ i ];
            last = input[ i ];
        }
        if ( input[ i ] != ',' )
            bdd.push_back( input[ i ] );
    }
    //cout << size;
    /*for ( int i = 0; i < bdd.size(); ++i )
     {
     cout << bdd[ i ] << endl;
     }*/
    size -= 96;
    table = NEW2D( pow( 2, size ), 1, bool );
    /*for ( int i = 0; i < pow( 2, size ); ++i )
    {
        for ( int j = 0; j < 1; ++j )
        {
            cout << table[ i ][ j ];
        }
        cout << endl;
    }*/
    
    for ( int i = 0; i < bdd.size(); ++i )
    {
        switch ( bdd[ i ] )
        {
            case '(':
                stack.push_back( '(' );
                break;
            case ')':
                while ( stack.back() != '(' )
                {
                    stack.pop_back();
                }
                stack.pop_back();
                break;
            case '0':
                for ( int i = 0; i <= last - stack.back(); ++i )
                {
                    cout << 0;
                }
                break;
            case '1':
                for ( int i = 0; i <= last - stack.back(); ++i )
                {
                    cout << 1;
                }
                break;
            default:
                stack.push_back( bdd[ i ] );
                break;
        }
    }
    return 0;
}
