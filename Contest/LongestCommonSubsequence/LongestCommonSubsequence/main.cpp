#include <iostream>
#include <string>
using namespace std;

int LCS( string s1, string s2 )
{
    unsigned long l1 = s1.length();
    unsigned long l2 = s2.length();
    
    if ( !( l1 && l2 ) )
        return 0;
    if ( s1[ 0 ] == s2[ 0 ] )
        return LCS( s1.substr( 1, l1 ), s2.substr( 1, l2 ) ) + 1;
    else
        return max( LCS( s1.substr( 1, l1 ), s2 ), LCS( s2.substr( 1, l2 ), s1 ) );
}
int main()
{
    string s1, s2;
    
    cin >> s1 >> s2;
    cout << LCS( s1, s2 ) << endl;
    
    return 0;
}