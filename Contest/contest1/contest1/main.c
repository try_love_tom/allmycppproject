#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int size;
int end = 1;
int lastX = 0;
int lastY = 0;
char *starMap;

void printMap( char *map )
{
    int x, y;
    
    for ( y = 0; y < size; ++y )
    {
        for ( x = 0; x < size; ++x )
        {
            printf( "%c", *( map + y * size + x ) );
        }
        printf( "\n" );
    }
}

void walk( char *map, int x, int y )
{
    *( map + y * size + x ) = '#';
    if ( ( x == size - 2 ) && ( y == size - 2 ) )
    {
        if ( end )
        {
            printMap( map );
            end = 0;
        }
    }
    else
    {
        // DOWN
        if ( *( map + y * size + x + size ) == '0' )
        {
            walk( map, x, y + 1 );
            if ( *( map + y * size + x + size ) == '#' )
            {
                *( map + y * size + x + size ) = '*';
            }
        }
        // UP
        if ( *( map + y * size + x - size ) == '0' )
        {
            walk( map, x, y - 1 );
            if ( *( map + y * size + x - size ) == '#' )
            {
                *( map + y * size + x - size ) = '*';
            }
        }
        // LEFT
        if ( *( map + y * size + x - 1 ) == '0' )
        {
            walk( map, x - 1, y );
            if ( *( map + y * size + x - 1 ) == '#' )
            {
                *( map + y * size + x - 1 ) = '*';
            }
        }
        // RIGHT
        if ( *( map + y * size + x + 1 ) == '0' )
        {
            walk( map, x + 1, y );
            if ( *( map + y * size + x + 1 ) == '#' )
            {
                *( map + y * size + x + 1 ) = '*';
            }
        }
    }
}

int main()
{
    char *map;
    
    scanf( "%d", &size );
    map = (char*)malloc( size * size );
    scanf( "%s", map );
    walk( map, 1, 1 );
    return 0;
}