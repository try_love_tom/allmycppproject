//
//  Network Cost.cpp
//  Problem B
//
//  Created by 張子晏 on 2014/12/25.
//  Copyright (c) 2014年 張子晏. All rights reserved.
//

#include <iostream>
#include <cmath>
#define MAX 99999
using namespace std;

struct point
{
    int x, y;
};
int main()
{
    int times;
    int n;
    
    cin >> times;
    while ( times-- )
    {
        cin >> n;
        point map[ n ];
        
        for ( int i = 0; i < n; ++i )
        {
            cin >> map[ i ].x >> map[ i ].y;
        }
        int cost[ n ][ n ];
        
        for ( int i = 0; i < n; ++i )
            for ( int j = 0; j < n; ++j )
                cost[ i ][ j ] = abs( map[ i ].x - map[ j ].x ) + abs( map[ i ].y - map[ j ].y );
        
        int totalA = 0, totalB = 0;
        
        for ( int i = 0; i < n; ++i )
        {
            int minA = MAX;
            int minB = MAX;
            for ( int j = 0; j < n; ++j )
            {
                if ( j > i )
                {
                    if ( cost[ i ][ j ] < minA )
                        minA = cost[ i ][ j ];
                }
                else if ( j < i )
                {
                    if ( cost[ i ][ j ] < minB )
                        minB = cost[ i ][ j ];
                }
            }
            if ( i != n - 1 )
                totalA += minA;
            if ( i != 0 )
                totalB += minB;
        }
        cout << ( ( totalA < totalB ) ? totalA : totalB ) << endl;
    }
    return 0;
}