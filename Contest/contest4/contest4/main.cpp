#include <iostream>
#include <string>
#include <vector>
#define SIZE 
using namespace std;

int currentSelection = 0;
int hasAns = 0;

struct point
{
    int x;
    int y;
};

vector< point > myVector;

point pointMaker( int y, int x )
{
    point temp;
    temp.x = x;
    temp.y = y;
    return temp;
}
bool pointCompare( point a, point b )
{
    return ( a.x == b.x ) && ( a.y == b.y );
}
void printMap( int map[ SIZE + 2 ][ SIZE + 2 ] )
{
    for ( int i = 1; i <= SIZE; ++i )
    {
        for ( int j = 1; j <= SIZE; ++j )
        {
            if ( map[ i ][ j ] == CROSS )
            {
                cout << "+";
            }
            else
            {
                cout << map[ i ][ j ];
            }
        }
        cout << endl;
    }
    cout << "\nTotal: " << hasAns << endl << endl;
}
void walk( int map[ SIZE + 2 ][ SIZE + 2 ], point currentPoint, point finalPoint )
{
    if ( pointCompare( currentPoint, finalPoint ) )
    {
        currentSelection += 2;
        if ( currentSelection < myVector.size() )
        {
            walk( map, myVector.at( currentSelection ), myVector.at( currentSelection + 1 ) );
        }
        else
        {
            bool isAns = true;
            
            for ( int i = 1; i <= SIZE; ++i )
            {
                for ( int j = 1; j <= SIZE; ++j )
                {
                    if ( map[ i ][ j ] == 0 )
                    {
                        isAns = false;
                    }
                }
            }
            if ( isAns )
            {
                ++hasAns;
                printMap( map );
            }
        }
        currentSelection -= 2;
    }
    else
    {
        int currentMap[ SIZE + 2 ][ SIZE + 2 ];
        
        memcpy( currentMap, map, sizeof( currentMap ) );
        // Up
        if ( currentMap[ currentPoint.y - 1 ][ currentPoint.x ] == 0 || pointCompare( pointMaker( currentPoint.y - 1, currentPoint.x ), finalPoint ) )
        {
            currentMap[ currentPoint.y - 1 ][ currentPoint.x ] = currentMap[ currentPoint.y ][ currentPoint.x ];
            walk( currentMap, pointMaker( currentPoint.y - 1, currentPoint.x ), finalPoint );
        }
        // Up cross
        if ( currentMap[ currentPoint.y - 1 ][ currentPoint.x ] == CROSS )
        {
            if ( currentMap[ currentPoint.y - 2 ][ currentPoint.x ] == 0 || pointCompare( pointMaker( currentPoint.y - 2, currentPoint.x ), finalPoint ) )
            {
                currentMap[ currentPoint.y - 2 ][ currentPoint.x ] = currentMap[ currentPoint.y ][ currentPoint.x ];
                walk( currentMap, pointMaker( currentPoint.y - 2, currentPoint.x ), finalPoint );
            }
        }
        memcpy( currentMap, map, sizeof( currentMap ) );
        // Right
        if ( currentMap[ currentPoint.y ][ currentPoint.x + 1 ] == 0 || pointCompare( pointMaker( currentPoint.y, currentPoint.x + 1 ), finalPoint ) )
        {
            currentMap[ currentPoint.y ][ currentPoint.x + 1 ] = currentMap[ currentPoint.y ][ currentPoint.x ];
            walk( currentMap, pointMaker( currentPoint.y, currentPoint.x + 1 ), finalPoint );
        }
        // Right cross
        if ( currentMap[ currentPoint.y ][ currentPoint.x + 1 ] == CROSS )
        {
            if ( currentMap[ currentPoint.y ][ currentPoint.x + 2 ] == 0 || pointCompare( pointMaker( currentPoint.y, currentPoint.x + 2 ), finalPoint ) )
            {
                currentMap[ currentPoint.y ][ currentPoint.x + 2 ] = currentMap[ currentPoint.y ][ currentPoint.x ];
                walk( currentMap, pointMaker( currentPoint.y, currentPoint.x + 2 ), finalPoint );
            }
        }
        memcpy( currentMap, map, sizeof( currentMap ) );
        // Down
        if ( currentMap[ currentPoint.y + 1 ][ currentPoint.x ] == 0 || pointCompare( pointMaker( currentPoint.y + 1, currentPoint.x ), finalPoint ) )
        {
            currentMap[ currentPoint.y + 1 ][ currentPoint.x ] = currentMap[ currentPoint.y ][ currentPoint.x ];
            walk( currentMap, pointMaker( currentPoint.y + 1, currentPoint.x ), finalPoint );
        }
        // Down cross
        if ( currentMap[ currentPoint.y + 1 ][ currentPoint.x ] == CROSS )
        {
            if ( currentMap[ currentPoint.y + 2 ][ currentPoint.x ] == 0 || pointCompare( pointMaker( currentPoint.y + 2, currentPoint.x ), finalPoint ) )
            {
                currentMap[ currentPoint.y + 2 ][ currentPoint.x ] = currentMap[ currentPoint.y ][ currentPoint.x ];
                walk( currentMap, pointMaker( currentPoint.y + 2, currentPoint.x ), finalPoint );
            }
        }
        memcpy( currentMap, map, sizeof( currentMap ) );
        // Left
        if ( currentMap[ currentPoint.y ][ currentPoint.x - 1 ] == 0 || pointCompare( pointMaker( currentPoint.y, currentPoint.x - 1 ), finalPoint ) )
        {
            currentMap[ currentPoint.y ][ currentPoint.x - 1 ] = currentMap[ currentPoint.y ][ currentPoint.x ];
            walk( currentMap, pointMaker( currentPoint.y, currentPoint.x - 1 ), finalPoint );
        }
        // Left cross
        if ( currentMap[ currentPoint.y ][ currentPoint.x - 1 ] == CROSS )
        {
            if ( currentMap[ currentPoint.y ][ currentPoint.x - 2 ] == 0 || pointCompare( pointMaker( currentPoint.y, currentPoint.x - 2 ), finalPoint ) )
            {
                currentMap[ currentPoint.y ][ currentPoint.x - 2 ] = currentMap[ currentPoint.y ][ currentPoint.x ];
                walk( currentMap, pointMaker( currentPoint.y, currentPoint.x - 2 ), finalPoint );
            }
        }
    }
}
void findPoint( int map[ SIZE + 2 ][ SIZE + 2 ] )
{
    for ( int i = 1; i <= SIZE; ++i )
    {
        for ( int j = 1; j <= SIZE; ++j )
        {
            if ( ( map[ i ][ j ] != 0 ) && map[ i ][ j ] != CROSS )
            {
                int k;
                
                for ( k = 0; k < myVector.size(); k += 2 )
                {
                    if ( map[ myVector.at( k ).y ][ myVector.at( k ).x ] == map[ i ][ j ] )
                    {
                        myVector.at( k + 1 ) = pointMaker( i, j );
                        break;
                    }
                }
                if ( k >= myVector.size() )
                {
                    myVector.push_back( pointMaker( i, j ) );
                    myVector.push_back( pointMaker( 0, 0 ) );
                }
            }
        }
    }
}
int main()
{
    int map[ SIZE + 2 ][ SIZE + 2 ] =
    {
        { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 },
        { 9, 0, 0, 0, 0, 1, 0, 0, 2, 9 },
        { 9, 0, 0, 0, 0, 0, 0, 0, 3, 9 },
        { 9, 1, 8, 7, 0, 0, 0, 3, 4, 9 },
        { 9, 4, 0, 0, 0, 0, 0, 2, 0, 9 },
        { 9, 5, 6, 0, 0, 0, 0, 8, 0, 9 },
        { 9, 0, 0, 0, 0, 0, 0, 7, 0, 9 },
        { 9, 0, 0, CROSS, 0, 0, 0, 6, 0, 9 },
        { 9, 0, 5, 0, 0, 0, 0, 0, 0, 9 },
        { 9, 9, 9, 9, 9, 9, 9, 9, 9, 9 }
    };
    printMap( map );
    findPoint( map );
    if ( myVector.size() )
    {
        walk( map, myVector.at( currentSelection ), myVector.at( currentSelection + 1 ) );
    }
    if ( !hasAns )
    {
        cout << "NONE" << endl;
    }
    return 0;
}