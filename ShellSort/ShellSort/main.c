//
//  main.c
//  ShellSort
//
//  Created by ChangTom on 2015/10/22.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#define N 10000

int *insertionSort( int *array, int length, int dist )
{
    int temp = 0;
    
    if ( dist <= 0 )
    {
        dist = 1;
    }
    
    for ( int index = 0; index < length; index += dist )
    {
        temp = *( array + index );
        
        int counter = index - dist;
        
        for ( ; counter >= 0 && *( array + counter ) > temp ; counter -= dist )
        {
            *( array + counter + dist ) = *( array + counter );
        }
        
        *( array + counter + dist ) = temp;
    }
    
    return array;
}

int findGapK( int length )
{
    int k = 0;
    double temp = 0;
    
    while ( temp < length )
    {
        k++;
        temp = (int)( pow( 4, k ) + 3 * pow( 2, k ) + 1 );
    }
    
    return k - 1;
}

int *shellSort( int *array, int length )
{
    int k = findGapK( length );
    
    for ( int gap = (int)( pow( 4, k ) + 3 * pow( 2, k ) + 1 ); k >= -1; )
    {
        insertionSort( array, length, gap );
        k--;
        if ( k < 0 )
        {
            gap = 1;
        }
        else
        {
            gap = (int)( pow( 4, k ) + 3 * pow( 2, k ) + 1 );
        }
    }
    
    return array;
}

int main( int argc, const char * argv[] )
{
    int size = N;
    int flip = 1;
    
    while ( 1 )
    {
        FILE *fp = fopen( "log", "a") ;     /* open file pointer */
        
        int *list = (int *)malloc( sizeof( int ) * size );
        
        for ( int i = 0; i < size; ++i )
        {
            *( list + i ) = size - i;
        }
        
        clock_t t;
        
        t = clock();
        shellSort( list, size );
        t = clock() - t;
        
        printf( "Please type input size(n): %d\nElapsed time: %lf(s)\n",size , ( (double)t ) / CLOCKS_PER_SEC );
        fprintf( fp, "Please type input size(n): %d\nElapsed time: %lf(s)\n",size , ( (double)t ) / CLOCKS_PER_SEC );
        
        if ( flip )
        {
            size *= 5;
        }
        else
        {
            size *= 2;
        }
        flip = !flip;
        
        free( list );
        fclose( fp );
    }
    return 0;
}
