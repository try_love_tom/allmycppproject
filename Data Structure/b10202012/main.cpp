// Data Structure Homework 02 Version 5 by Tom Chang
#include <iostream>
#include <iomanip>
using namespace std;

class node
{
public:
    node *left;
    node *right;
    int value;
    
    node( int input )
    {
        left = nullptr;
        right = nullptr;
        value = input;
    }
};
class BinarySearchTree
{
public:
    node *root;
    
    BinarySearchTree()
    {
        root = nullptr;
    }
    ~BinarySearchTree()
    {
        deleteTree( root );
    }
    void deleteTree( node *subTreeRoot )
    {
        if ( subTreeRoot )
        {
            deleteTree( subTreeRoot->left );
            deleteTree( subTreeRoot->right );
            delete subTreeRoot;
        }
        subTreeRoot = nullptr;
    }
    void insertNode( node *subTreeRoot, node *node  )
    {
        if ( subTreeRoot )
        {
            if ( node->value > subTreeRoot->value )
            {
                if ( !subTreeRoot->right )
                    subTreeRoot->right = node;
                else
                    insertNode( subTreeRoot->right, node );
            }
            else if ( node->value < subTreeRoot->value )
            {
                if ( !subTreeRoot->left )
                    subTreeRoot->left = node;
                else
                    insertNode( subTreeRoot->left, node );
            }
            else
                cout << "Exist!!" << endl;
        }
        else
            root = node;
    }
    void printInO( node *subTreeRoot )
    {
        if ( subTreeRoot )
        {
            printInO( subTreeRoot->left );
            cout << subTreeRoot->value << ' ';
            printInO( subTreeRoot->right );
        }
    }
    void print( node *subTreeRoot, int depth = 1 )
    {
        if ( subTreeRoot )
        {
            print( subTreeRoot->left, depth + 2 );
            for ( int i = 1; i < depth; ++i )
                cout << "  ";
            cout << subTreeRoot->value << "<" << endl;
            print( subTreeRoot->right, depth + 2 );
        }
    }
    void deleteLowestNode()
    {
        if ( root )
        {
            if ( root->left )
            {
                node *currentNode = root;
                
                while ( currentNode->left->left )
                    currentNode = currentNode->left;
                if ( currentNode->left->right )
                    currentNode->left = currentNode->left->right;
                else
                {
                    delete ( currentNode->left );
                    currentNode->left = nullptr;
                }
            }
            else if ( root->right )
            {
                node *temp = root;
                
                root = root->right;
                delete temp;
            }
            else
            {
                delete root;
                root = nullptr;
            }
        }
    }
};
int main()
{
    int input;
    bool trans = false;
    BinarySearchTree *BST = new BinarySearchTree();
    
    while ( 1 )
    {
        cout << "======================================\n" << endl;
        if ( !BST->root )
            cout << "Empty" << endl;
        else
        {
            BST->print( BST->root );
        }
        if ( trans && BST->root )
        {
            cout << endl;
            BST->printInO( BST->root );
            trans = false;
        }
        cout << "\n======================================\n";
        cout << "Enter the number to chose operation:\n"
        << "[1] Create an empty binary search tree.\n"
        << "[2] Insert a key into a binary search tree.\n"
        << "[3] Remove the key with the lowest value.\n"
        << "[4] Print out all keys in increasing order.\n"
        << "[5] Exit.\n>> ";
        cin >> input;
        switch ( input )
        {
            case 1:
                cout << "Create a new tree..." << endl;
                BST->~BinarySearchTree();
                BST = new BinarySearchTree();
                break;
            case 2:
                do
                {
                    cout << "======================================\n" << endl;
                    if ( !BST->root )
                        cout << "Empty" << endl;
                    else
                        BST->print( BST->root );
                    cout << "\n======================================\n";
                    cout << "Enter the integer value of root below." << endl
                    << "If you input another thing, operation will be finish.\n"
                    << ">> ";
                    cin >> input;
                    if ( !cin.good() )
                    {
                        cin.clear();
                        cin.ignore( 256, '\n' );
                        break;
                    }
                    else
                        BST->insertNode( BST->root, new node( input ) );
                } while ( 1 );
                break;
            case 3:
                BST->deleteLowestNode();
                break;
            case 4:
                trans = true;
                break;
            case 5:
                BST->~BinarySearchTree();
                exit( 0 );
                break;
            default:
                cout << "Wrong input!!" << endl;
                cin.clear();
                cin.ignore( 256, '\n' );
        }
    }
    return 0;
}