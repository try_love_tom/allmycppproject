//
//  main.cpp
//  OrthogonalBasis
//
//  Created by 張子晏 on 2015/6/28.
//  Copyright (c) 2015年 張子晏. All rights reserved.
//

#include <iostream>
#include <vector>
#include <math.h>
#include <iomanip>
using namespace std;

vector< double > null;
vector< vector< double > > OB( vector< vector< double > > x );
vector< double > reduce_vec( vector< double > x, vector< double > y );
vector< double > divid_vec( vector< double > x, vector< double > y );
vector< double > Projection( vector< double > x, vector< double > y );
vector< double > dot_vec( vector< double > x, vector< double > y);
double Component( vector< double > x, vector< double > y );
double Norm( vector< double > x );

int main( int argc, const char * argv[] )
{
    // insert code here...
    cout << "Hello, World!\n";
    null.clear();
    
    vector< double > a = { -68.358043, -71.770035, 85.769027 };
    vector< double > b = { -42.710827, 2.417330, 46.420770 };
    vector< double > c = { 37.425823, 44.265385, 49.969604 };
    vector< vector< double > > test = { a, b, c };
    
    vector< vector< double > > output = OB( test );
    
    for ( unsigned i = 0; i < output.size(); ++i )
    {
        for ( unsigned j = 0; j < output[ i ].size(); ++j )
        {
            cout << setw( 15 ) <<  output[ i ][ j ];
        }
        cout << endl;
    }
    
    return 0;
}

vector< vector< double > > OB( vector< vector< double > > x )
{
    vector< vector< double > > a;
    a.push_back( x[ 0 ] );
    
    for ( unsigned i = 1; i < x.size(); i++ )
    {
        vector< double > temp_x = x[ i ];
        for ( unsigned j = 0; j < i; j++ )
        {
            temp_x = reduce_vec( temp_x, Projection( x[ i ], a[ j ] ) );
        }
        a.push_back( temp_x );
    }
    
    for ( unsigned i = 0; i < a.size(); i++ )
    {
        vector< double > temp;
        temp.push_back( Norm( a[ i ] ) );
        a[ i ] = divid_vec( a[ i ], temp );
    }
    return a;
}

vector< double > Projection( vector< double > x, vector< double > y )
{
    vector< double > sum, numerator;
    numerator.push_back( Component( x, y ) );
    for ( unsigned k = 0 ; k < y.size(); k++ )
    {
        sum.push_back( dot_vec( numerator , y )[ k ] / Norm( y ) );
    }
    return sum;
}

vector< double > reduce_vec( vector< double > x, vector< double > y )
{
    vector< double > sum;
    
    if ( x.size() != y.size() )
    {
        return null;
    }
    
    for ( unsigned i = 0; i < x.size(); i++ )
    {
        sum.push_back( x [ i ] - y[ i ] );
    }
    return sum;
}

vector< double > divid_vec( vector< double > x, vector< double > y )
{
    vector< double > sum;
    
    if ( x.size() != 1 && y.size() != 1 && x.size() != 0 && y.size() != 0 )
    {
        return null;
    }
    if ( x.size() == 1 )
    {
        for ( unsigned i = 0; i < y.size() ; i++)
        {
            sum.push_back( y[ i ] / x[ 0 ]);
        }
    }
    if ( y.size() == 1 )
    {
        for ( unsigned i = 0; i < x.size() ; i++)
        {
            sum.push_back( x[ i ] / y[ 0 ]);
        }
    }
    return sum;
}

double Norm( vector< double > x )
{
    double sum = 0;
    for ( unsigned i = 0; i < x.size(); i++ )
    {
        sum += pow( x[ i ], 2 );
    }
    return sqrt( sum );
}

vector< double > dot_vec( vector< double > x, vector< double > y )
{
    vector< double > sum;
    double dot = 0;
    
    if ( x.size() == 1 && y.size() == 1 )
    {
        sum.push_back( x[ 0 ] * y[ 0 ] );
        return sum;
    }
    
    if ( x.size() == 1 && y.size() != 1 )
    {
        for ( unsigned i = 0; i < y.size(); i++ )
            sum.push_back( x[ 0 ] * y[ i ] );
        return sum;
    }
    
    if( y.size() == 1 && x.size() != 1 )
    {
        for ( unsigned i = 0; i < x.size(); i++ )
        {
            sum.push_back( y[ 0 ] * x[ i ] );
        }
        return sum;
    }
    
    if( x.size() != y.size() )
    {
        return null;
    }
    
    for ( unsigned i = 0; (unsigned)i < x.size(); i++ )
    {
        dot += x[ i ] * y[ i ];
    }
    
    sum.push_back( dot );
    
    return sum;
}

double Component( vector< double > x, vector< double > y )
{
    if ( x.size() == 1 || x.size() == 1)
    {
        return -1;
    }
    return dot_vec( x, y ).front() / Norm( y );
}
