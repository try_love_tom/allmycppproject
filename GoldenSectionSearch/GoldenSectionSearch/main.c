//
//  main.c
//  GoldenSectionSearch
//
//  Created by ChangTom on 2015/10/26.
//  Copyright © 2015年 ChangTom. All rights reserved.
//

#include <stdio.h>
#include <math.h>
#include <limits.h>

double gr = 0.0;

double findGr()
{
    return gr = ( sqrt( 5 ) - 1 ) / 2;
}

double gss( double (*f)( double ), double a, double b, double tol, int isMin )
{
    if ( gr == 0.0 )
    {
        findGr();
    }
    
    double c = b - gr * ( b - a );
    double d = a + gr * ( b - a );
    
    while ( fabs( c - d ) > tol )
    {
        double fc = f( c );
        double fd = f( d );
        
        if ( isMin )
        {
            if ( fc < fd )
            {
                b = d;
                d = c; // fd = fc; fc = f( c )
                c = b - gr * ( b - a );
            }
            else
            {
                a = c;
                c = d; // fc = fd; fd = f( d )
                d = a + gr * ( b - a );
            }
        }
        else
        {
            if ( fc > fd )
            {
                b = d;
                d = c; // fd = fc; fc = f( c )
                c = b - gr * ( b - a );
            }
            else
            {
                a = c;
                c = d; // fc = fd; fd = f( d )
                d = a + gr * ( b - a );
            }
        }
    }
    
    return ( b + a ) / 2;
}

double f( double x )
{
    return 9 * exp( -x ) * sin( 2 * M_PI * x ) - 3.5;
}

double BruteForceStepwise( double(*f)( double ), double a, double b, double tol, int isMin )
{
    double precise = ( a + b ) / 2;
    double result;
    double x;
    
    if ( isMin )
    {
        result = 999999999;
    }
    else
    {
        result = -999999999;
    }
    
    while ( precise >= tol )
    {
        for ( double i = a; i <= b; i += precise )
        {
            double fi = f ( i );
            
            if ( isMin )
            {
                if ( f( i ) < result )
                {
                    result = fi;
                    x = i;
                }
            }
            else
            {
                if ( f( i ) > result )
                {
                    result = fi;
                    x = i;
                }
            }
        }
        precise /= 10;
    }
    return x;
}

int main( int argc, const char * argv[] )
{
    double resultMin = gss( &f, 0, 2, pow( 10 , -8 ), 1 );
    double resultMax = gss( &f, 0, 1, pow( 10 , -8 ), 0 );
    
    printf( "Min: x = %lf y = %lf\n", resultMin, f( resultMin ) );
    printf( "Max: x = %lf y = %lf\n", resultMax, f( resultMax ) );
    
    return 0;
}
